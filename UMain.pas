unit UMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

const
        ADICAO='+';
        SUBTRACAO='-';
        DIVISAO='/';
        MULTIPLICACAO='*';
        IGUAL='=';
        ZERO='0';
        VIRGULA=',';
        PORCENTAGEM='%';

type
  TFrmCalculadora = class(TForm)
    mmoHistorico: TMemo;
    edtVisor: TEdit;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn8: TButton;
    btn9: TButton;
    btn7: TButton;
    btn5: TButton;
    btn6: TButton;
    btn4: TButton;
    btnSubtracao: TButton;
    btnDivisao: TButton;
    btnMultiplicacao: TButton;
    btnInversao: TButton;
    btnPorcentagem: TButton;
    btn0: TButton;
    btnVirgula: TButton;
    btnAdicao: TButton;
    btnIgual: TButton;
    edtCalculo: TEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btnClickNumero(Sender:TObject);
    procedure apagaUltimoCaracter(edit:TEdit);
    procedure edtVisorChange(Sender: TObject);
    procedure validaEntrada(edt :TEdit;Key:Char);
    procedure btnVirgulaClick(Sender: TObject);
    procedure insereValidaVirgula;
    procedure inserirNumero(txt:string);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure btnClickOperacao(Sender: TObject);
    procedure btnIgualClick(Sender: TObject);
    procedure btnPorcentagemClick(Sender: TObject);
  private
    { Private declarations }
    ultimoValor:Double;
    ultimaOperacao:Char;
    inicioCalculo:Boolean;
    zerarVisor:Boolean;
    historico :string;
    function executaOperacaoAritmetica(operacao:Char;a,b:Double):Double;

  public
    { Public declarations }
  end;

var
  FrmCalculadora: TFrmCalculadora;

implementation

{$R *.dfm}

procedure TFrmCalculadora.apagaUltimoCaracter(edit: TEdit);
begin
  edit.Text:= Copy(edit.Text,1,Length(edtVisor.Text)-1);
end;

procedure TFrmCalculadora.btnClickNumero(Sender: TObject);
begin
  inserirNumero(TButton(Sender).Caption);
end;

procedure TFrmCalculadora.btnClickOperacao(Sender: TObject);
var
        valorVisor:Double;
begin
        valorVisor := StrToFloat(edtVisor.Text);

        if inicioCalculo then
        Begin
                ultimoValor := valorVisor;
                ultimaOperacao := TButton(Sender).Caption[1];
                inicioCalculo:= False;
                historico := edtVisor.Text + ultimaOperacao;
                edtVisor.Text:=ZERO;


        End
        else
        Begin
                ultimoValor := executaOperacaoAritmetica(ultimaOperacao,ultimoValor,valorVisor);
                ultimaOperacao:= TButton(Sender).Caption[1];
                historico := historico+edtVisor.Text+ultimaOperacao;
                edtVisor.Text := ZERO;
        End;

        edtCalculo.Text := historico;


end;

procedure TFrmCalculadora.btnIgualClick(Sender: TObject);
Var
        resultado:Double;
begin
        if ultimaOperacao<>IGUAL then
        Begin
                resultado:= executaOperacaoAritmetica(ultimaOperacao,ultimoValor,StrToFloat(edtVisor.Text));
                inicioCalculo := True;
                historico:= historico +edtVisor.Text+IGUAL+FloatToStr(resultado);
                mmoHistorico.Lines.Add(historico);
                edtVisor.Text := EmptyStr;
                ultimoValor := 0;
                edtCalculo.Text :=FloatToStr(resultado);
                historico :=EmptyStr;
                ultimaOperacao :=IGUAL;
        End;

 end;

procedure TFrmCalculadora.btnPorcentagemClick(Sender: TObject);
var
        percentual:Double;
        ultimaOperacaoAux :Char;
begin
        percentual:= ultimoValor*(StrToFloat(edtVisor.Text)/100);
        ultimoValor :=executaOperacaoAritmetica(ultimaOperacao,ultimoValor,percentual);
        historico:= historico+FloatToStr(percentual)+'%';
        edtCalculo.Text :=FloatToStr(percentual);
        edtVisor.Text := edtCalculo.Text;
        ultimaOperacao:= PORCENTAGEM;
end;

procedure TFrmCalculadora.btnVirgulaClick(Sender: TObject);
begin
  insereValidaVirgula;
end;

procedure TFrmCalculadora.edtVisorChange(Sender: TObject);
begin
        if edtVisor.Text=EmptyStr then
                edtVisor.Text:=ZERO;

end;

procedure TFrmCalculadora.FormCreate(Sender: TObject);
begin
        inicioCalculo := true;
        ReportMemoryLeaksOnShutdown := True;
end;

procedure TFrmCalculadora.FormKeyPress(Sender: TObject; var Key: Char);
begin
        validaEntrada(edtVisor,Key);
        btnIgual.SetFocus;
end;

procedure TFrmCalculadora.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
        btnIgual.SetFocus
end;

procedure TFrmCalculadora.inserirNumero(txt: string);
begin
        if edtVisor.Text=ZERO then
                edtVisor.Text := txt
        else
                edtVisor.Text := edtVisor.Text+txt;
end;

procedure TFrmCalculadora.insereValidaVirgula;
begin
        if Pos(VIRGULA,edtVisor.Text,1)=0 then
        Begin
                edtVisor.Text:=edtVisor.Text+VIRGULA;
        End;
end;

function TFrmCalculadora.executaOperacaoAritmetica(operacao: Char; a,
  b: Double): Double;
begin
        Result :=0;
        case operacao of
                ADICAO:
                        Result:= a+b;
                SUBTRACAO:
                        Result:= a-b;
                MULTIPLICACAO:
                        Result:= a*b;
                DIVISAO:
                        Result:= a/b
        end;
end;

procedure TFrmCalculadora.validaEntrada(edt: TEdit; Key: Char);
var
        caracteres :TSysCharSet;
begin
                caracteres:= ['0'..'9'];
                if CharInSet(Key,caracteres) then
                        inserirNumero(Key)
                else
                if Key=VIRGULA then
                        insereValidaVirgula
                else
                if Key=#$1B then  //esc
                Begin
                        edt.Text :=ZERO;
                        inicioCalculo := True;
                        edtCalculo.Text :=EmptyStr;
                End
                else
                if Key=#8 then //backspace
                        apagaUltimoCaracter(edtVisor)
                else
                if Key=ADICAO then
                     btnAdicao.Click
                else
                if Key=SUBTRACAO then
                     btnSubtracao.Click
                else
                if Key=MULTIPLICACAO then
                     btnMultiplicacao.Click
                else
                if Key=DIVISAO then
                     btnDivisao.Click;

                btnIgual.SetFocus;
end;



end.
