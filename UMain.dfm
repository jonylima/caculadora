object FrmCalculadora: TFrmCalculadora
  Left = 0
  Top = 0
  AutoSize = True
  Caption = 'Calculadora'
  ClientHeight = 338
  ClientWidth = 246
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object mmoHistorico: TMemo
    Left = 0
    Top = 0
    Width = 246
    Height = 91
    ReadOnly = True
    TabOrder = 6
  end
  object edtVisor: TEdit
    Left = 0
    Top = 110
    Width = 246
    Height = 21
    Alignment = taRightJustify
    Enabled = False
    TabOrder = 19
    Text = '0'
    OnChange = edtVisorChange
  end
  object btn1: TButton
    Left = 0
    Top = 239
    Width = 50
    Height = 50
    Caption = '1'
    TabOrder = 4
    OnClick = btnClickNumero
  end
  object btn2: TButton
    Left = 49
    Top = 239
    Width = 50
    Height = 50
    Caption = '2'
    TabOrder = 5
    OnClick = btnClickNumero
  end
  object btn3: TButton
    Left = 98
    Top = 239
    Width = 50
    Height = 50
    Caption = '3'
    TabOrder = 7
    OnClick = btnClickNumero
  end
  object btn8: TButton
    Left = 49
    Top = 140
    Width = 50
    Height = 50
    Caption = '8'
    TabOrder = 15
    OnClick = btnClickNumero
  end
  object btn9: TButton
    Left = 98
    Top = 140
    Width = 50
    Height = 50
    Caption = '9'
    TabOrder = 16
    OnClick = btnClickNumero
  end
  object btn7: TButton
    Left = 0
    Top = 140
    Width = 50
    Height = 50
    Caption = '7'
    TabOrder = 14
    OnClick = btnClickNumero
  end
  object btn5: TButton
    Left = 49
    Top = 189
    Width = 50
    Height = 51
    Caption = '5'
    TabOrder = 10
    OnClick = btnClickNumero
  end
  object btn6: TButton
    Left = 98
    Top = 189
    Width = 50
    Height = 51
    Caption = '6'
    TabOrder = 11
    OnClick = btnClickNumero
  end
  object btn4: TButton
    Left = 0
    Top = 189
    Width = 50
    Height = 51
    Caption = '4'
    TabOrder = 9
    OnClick = btnClickNumero
  end
  object btnSubtracao: TButton
    Left = 147
    Top = 239
    Width = 50
    Height = 50
    Caption = '-'
    TabOrder = 8
    OnClick = btnClickOperacao
  end
  object btnDivisao: TButton
    Left = 147
    Top = 140
    Width = 50
    Height = 50
    Caption = '/'
    TabOrder = 17
    OnClick = btnClickOperacao
  end
  object btnMultiplicacao: TButton
    Left = 147
    Top = 189
    Width = 50
    Height = 51
    Caption = '*'
    TabOrder = 12
    OnClick = btnClickOperacao
  end
  object btnInversao: TButton
    Left = 196
    Top = 189
    Width = 50
    Height = 51
    Caption = '1/x'
    TabOrder = 13
  end
  object btnPorcentagem: TButton
    Left = 196
    Top = 140
    Width = 50
    Height = 50
    Caption = '%'
    TabOrder = 18
    OnClick = btnPorcentagemClick
  end
  object btn0: TButton
    Left = 0
    Top = 288
    Width = 99
    Height = 50
    Caption = '0'
    TabOrder = 0
    OnClick = btnClickNumero
  end
  object btnVirgula: TButton
    Left = 98
    Top = 288
    Width = 50
    Height = 50
    Caption = ','
    TabOrder = 1
    OnClick = btnVirgulaClick
  end
  object btnAdicao: TButton
    Left = 147
    Top = 288
    Width = 50
    Height = 50
    Caption = '+'
    TabOrder = 2
    OnClick = btnClickOperacao
  end
  object btnIgual: TButton
    Left = 196
    Top = 239
    Width = 50
    Height = 99
    Caption = '='
    TabOrder = 3
    OnClick = btnIgualClick
  end
  object edtCalculo: TEdit
    Left = 0
    Top = 90
    Width = 246
    Height = 21
    ReadOnly = True
    TabOrder = 20
  end
end
